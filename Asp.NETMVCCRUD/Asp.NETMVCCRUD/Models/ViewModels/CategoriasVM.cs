﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asp.NETMVCCRUD.Models.ViewModels
{
    public class CategoriasVM
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}